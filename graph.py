import networkx as nx
from itertools import pairwise
from metric import knight_conn, knight_metric

class SGraph:
    def __init__(self, start, end):
        self.start = start
        self.end = end
        
        stuck = list(knight_conn(*start)) + list(knight_conn(*end))
        
        self.stuck = [x for x in stuck if x not in [self.start, self.end]]

        self.init_graph()
        
        self.paths_ = None
        self.loss_ = None
        
    def init_graph(self):
        nodes = [self.start] + [self.end] + self.stuck

        self.G = nx.Graph()

        self.G.add_nodes_from(nodes)

        for x in nodes:
            for y in nodes:
                self.G.add_edge(x, y, weight=knight_metric(*x, *y))
                
    def label_all_nodes(self):
        link_1 = nx.Graph()
        
        link_1.add_nodes_from(self.G.nodes)
        
        link_1.add_edges_from([(a,b) for a,b in self.G.edges if self.G.get_edge_data(a,b)["weight"] == 1])
        
        labels = {}
        
        for i, subgraph in enumerate(nx.connected_component_subgraphs(link_1)):
            for node in subgraph.nodes:
                labels[node] = i
                
        return labels

    def find_all_paths(self):
        if len(self.G.nodes) > 11:
            print("to many nodes")
            return None, ">"

        best = []
        all_ = []
        top = float("inf")

        for path in nx.all_simple_paths(self.G, source=self.start, target=self.end):
            if len(path) < len(self.G.nodes):
                continue
            lenght = sum(self.G.get_edge_data(a, b)["weight"] for a, b in pairwise(path))
            all_.append((path, lenght))
            if lenght > top:
                continue
            elif lenght == top:
                best.append(path)
                
            else:
                best[:] = [path]
                top = lenght

        self.all_paths_ = all_
        self.paths_ = best
        self.loss_ = top
        
    def paths(self):
        if self.paths_ is None:
            self.find_all_paths()
        return self.paths_
    
    def loss(self):
        if self.paths_ is None:
            self.find_all_paths()
        return self.loss_

