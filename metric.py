import numpy as np

mvs = []
for x in (-2, 2):
    for y in (-1, 1):
        mvs.extend([[x, y], [y, x]])

moves = np.array(mvs)

metric = {(0, 0): 0}


def sanitise_metric(x, y):
    if np.abs(x) > np.abs(y):
        return np.abs(x), np.abs(y)
    else:
        return np.abs(y), np.abs(x)


def targets(x, y):
    locs = np.array([x, y])[None, :]
    return locs + moves


for level in range(8):
    for k in [k for k, v in metric.items() if v == level]:
        for newloc in targets(*k):
            loc = sanitise_metric(*newloc)
            if loc not in metric and loc[0] < 8:
                metric[loc] = level + 1


def knight_metric(x1, y1, x2, y2):
    return metric[sanitise_metric(x2 - x1, y2 - y1)]


def knight_conn(x, y):
    for tx, ty in targets(x, y):
        if 0 <= tx < 8 and 0 <= ty < 8:
            yield tx, ty
