from itertools import chain, product, pairwise
from metric import knight_conn
import networkx as nx

fullgraph = nx.Graph()

for x in range(8):
  for y in range(8):
    fullgraph.add_node((x,y))

for x in range(8):
  for y in range(8):
    for t in knight_conn(x, y):
      fullgraph.add_edge((x,y), t)


def get_all_extends(path):
  paths_4 = [[l[:-1] for l in nx.shortest_paths.all_shortest_paths(fullgraph,source=s, target=t)] for s, t in pairwise(path)]
  for x in product(*paths_4):
    data = list(chain(*x)) + [path[-1]]
    if len(set(data)) == len(data):
      yield data
