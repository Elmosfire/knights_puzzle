import drawsvg as draw
import numpy as np
from itertools import chain


class CheckBoardVisualiser:
    def __init__(self, parent=None, xoffset=0, yoffset=0):
        if parent is None:
            self.d = draw.Drawing(400, 400)
        else:
            self.d = parent

        self.real_offset_x = xoffset * 420
        self.real_offset_y = yoffset * 420
        self.draw_board()

    def tx(self, x):
        return int(self.real_offset_x + x * 50)

    def ty(self, y):
        return int(self.real_offset_y + y * 50)

    def draw_board(self):
        for x in range(8):
            for y in range(8):
                col = "white" if (x + y) % 2 else "lightgrey"
                self.color_square(x, y, col)

    def color_square(self, x, y, color):
        r = draw.Rectangle(self.tx(x), self.ty(y), 50, 50, fill=color)
        self.d.append(r)

    def mark_square(self, x, y, color):
        r = draw.Rectangle(self.tx(x) + 15, self.ty(y) + 15, 20, 20, fill=color)
        self.d.append(r)

    def color_square_lst(self, lst, color):
        for x, y in lst:
            self.color_square(x, y, color)

    def mark_square_lst(self, lst, color):
        for x, y in lst:
            self.mark_square(x, y, color)

    def draw_marked_line(self, x1, y1, x2, y2, text, color="grey"):
        self.d.append(
            draw.Line(
                self.tx(x1) + 25,
                self.ty(y1) + 25,
                self.tx(x2) + 25,
                self.ty(y2) + 25,
                stroke=color,
                stroke_width=2,
            )
        )
        self.d.append(
            draw.Text(
                text,
                20,
                self.tx(x1 / 2 + x2 / 2) + 25,
                self.ty(y1 / 2 + y2 / 2) + 25,
                fill="black",
                text_anchor="middle",
                dominant_baseline="central",
            )
        )

    def draw_lines(self, lst):
        self.d.append(
            draw.Lines(
                *chain(*((self.tx(x) + 25, self.ty(y) + 25) for x, y in lst)),
                stroke="grey",
                stroke_width=2,
                close=False,
                fill="none"
            )
        )

    def draw_number(self, x, y, text):
        self.d.append(
            draw.Text(
                text,
                20,
                self.tx(x) + 25,
                self.ty(y) + 25,
                fill="black",
                text_anchor="middle",
                dominant_baseline="central",
            )
        )


class CheckBoardSet:
    def __init__(self, px, py):
        self.px = px
        self.py = py
        self.drawing = draw.Drawing(420 * px, 420 * py)

        self.index = 0

    def grab_board(self):
        xloc = self.index % self.px
        yloc = self.index // self.px

        self.index += 1

        return CheckBoardVisualiser(self.drawing, xloc, yloc)


if __name__ == "__main__":
    cv = CheckBoardSet(2, 2)

    cv.grab_board()
    cv.grab_board()
    cv.grab_board()

    cv.drawing.save_png("test.png")
